# Algorithms
=============================

## Includes Algorithms
A parser that scans the source file and adds the included content recursively.
It is efficient by not keeping the files open more than just to read them once.
