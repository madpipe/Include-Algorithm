#include <list>
#include <iterator>
#include <string>
#include <fstream>
#include <iostream>

class Parser
{
public:
    Parser();
private:
    typedef std::list<std::string> StringList;
    StringList buffer;
    StringList::iterator currentLine;

public:
    bool loadLines(std::string fileName);
    std::string getCurrentLine();
    bool hasNextLine();
    void resetCurrentLine();
    bool hasInclude(std::string line);
    std::string getIncludeFileName(std::string line);
    void removeIncludes();
};

int main (int argc, char** argv)
{
    Parser pr;
    pr.loadLines("inițial.txt");
    pr.resetCurrentLine();
    while (pr.hasNextLine())
    {
        std::string line = pr.getCurrentLine();
        if (pr.hasInclude(line))
        {
            pr.loadLines(pr.getIncludeFileName(line));
        }
    }
    pr. removeIncludes();

    // Print buffer
    pr.resetCurrentLine();
    while (pr.hasNextLine())
        std::cout << pr.getCurrentLine() << std::endl;
    return 0;
}

Parser::Parser()
{
    currentLine = buffer.begin();
}

std::string Parser::getCurrentLine()
{
    return *currentLine++;
}

bool Parser::hasNextLine()
{
    if (currentLine != buffer.end())
        return true;
    return false;
}

void Parser::resetCurrentLine()
{
    currentLine = buffer.begin();
}

bool Parser::hasInclude(std::string line)
{
    if (line.find(".include ") != std::string::npos)
        return true;
    return false;
}

std::string Parser::getIncludeFileName(std::string line)
{
    std::string inc = ".include ";
    std::size_t pos = line.find(inc);
    return line.substr(pos + inc.size());
}

bool Parser::loadLines(std::string fileName)
{
    if (std::ifstream(fileName.c_str()))
    {
        std::ifstream ifs(fileName.c_str());
        if (ifs.is_open())
        {
            std::string line;
            unsigned int oldLine = std::distance(buffer.begin(), currentLine);
            while (std::getline(ifs, line))
            {
                buffer.insert(currentLine, line);
            }
            resetCurrentLine();
            std::advance(currentLine, oldLine);
        }
    }
    else
    {
        std::cout << "ERROR: File " << fileName << " does not exist!" << std::endl;
    }
}

void Parser::removeIncludes()
{
    unsigned int oldLine = std::distance(buffer.begin(), currentLine);
    for (resetCurrentLine(); currentLine != buffer.end(); currentLine++)
    {
        if (hasInclude(*currentLine))
            buffer.erase(currentLine++);
    }
    std::advance(currentLine, oldLine);
}
